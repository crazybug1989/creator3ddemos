import { _decorator, Component, Node } from 'cc';
import { UIMgr, UILayer } from './UIMgr';
import { HUD } from './HUD';
const { ccclass, property } = _decorator;

@ccclass('AppStart')
export class AppStart extends Component {
    start () {
        UIMgr.inst.setup(UILayer.NUM);
        UIMgr.inst.showUI(HUD);
    }
}
